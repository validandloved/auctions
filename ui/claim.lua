local S = minetest.get_translator("auctions")

function auctions.show_claim(name)
    local inv_name = "claim:" .. name
    if not minetest.get_inventory({ type="detached", name=inv_name }) then
        minetest.create_detached_inventory(inv_name, {
            allow_put = function(inv, listname, index, stack, player)
                return 0 -- avoid putting (take-only inventory)
        end})
        local inv = minetest.get_inventory({ type="detached", name=inv_name })
        inv:set_size("main", 36)
        inv:set_width("main", 9)
    end
    local inv = minetest.get_inventory({ type="detached", name=inv_name })
    local items = auctions.get_claim(name)
    -- fill the temporary inventory with claim items
    if inv:is_empty("main") then
        for _, item in ipairs(items) do
            inv:add_item("main", ItemStack(item))
        end
    end

    local new_formspec = [[
        formspec_version[4]
        size[12,13;]

        label[0.5,5.5;]] .. S("Claim inventory") .. [[]
        label[0.5,6.25;]] .. S("Your inventory") .. [[]

        button[0.5,11.7;1.5,0.8;back;]] .. S("Back") .. [[]

        list[detached:]] .. inv_name .. [[;main;0.5,0.5;9,4]
        list[player:]] .. name .. [[;main;0.5,6.5;9,4]
        listring[]
        ]]
    local formspec = { new_formspec }

    minetest.show_formspec(name, "auctions:claim", table.concat(formspec, ""))
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "auctions:claim" then
        return
    end

    local name = player:get_player_name()
    local pinv = minetest.get_inventory({ type="player", name=name })
    local inv_name = "claim:" .. name
    local inv = minetest.get_inventory({ type="detached", name=inv_name })
    local stack_items = inv:get_list("main")
    if fields.back then
        auctions.show_board(name)
    end

    -- update claim in storage
    for _, s in ipairs(stack_items) do
        local items = {}
        -- convert itemstacks into tables
        for _, s in ipairs(stack_items) do
            table.insert(items, s:to_table())
        end
        auctions.set_claim(name, items)
    end

    return true
end)
