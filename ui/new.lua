local S = minetest.get_translator("auctions")

function auctions.show_new(name)
    local inv_name = "new:" .. name
    if not minetest.get_inventory({ type="detached", name=inv_name }) then
        minetest.create_detached_inventory(inv_name)
        local inv = minetest.get_inventory({ type="detached", name=inv_name })
        inv:set_size("main", 36)
        inv:set_width("main", 9)
    end
    local inv = minetest.get_inventory({ type="detached", name=inv_name })

    local new_formspec = [[
        formspec_version[4]
        size[17,12;]

        label[5.5,5.5;]] .. S("Auction inventory") .. [[]
        label[5.5,6.25;]] .. S("Your inventory") .. [[]

        field[0.5,1;4,0.8;title;]] .. S("Title") .. [[;]
        textarea[0.5,2.6;4,1.6;desc;]] .. S("Description") .. [[;]
        field[0.5,5.0;4,0.8;bid;]] .. S("Initial bid") .. [[;10]
        field[0.5,6.4;4,0.8;increment;]] .. S("Increment") .. [[;1]
        field[0.5,7.8;4,0.8;fee;]] .. S("Fee") .. [[;1]
        field[0.5,9.2;1,0.8;days;]] .. S("Days") .. [[;0]
        field[2.0,9.2;1,0.8;hours;]] .. S("Hours") .. [[;0]
        field[3.5,9.2;1,0.8;minuts;]] .. S("Minuts") .. [[;0]

        button[0.5,10.7;1.5,0.8;back;]] .. S("Back") .. [[]
        button[2.5,10.7;2,0.8;new;]] .. S("Create") .. [[]

        list[detached:]] .. inv_name .. [[;main;5.5,0.5;9,4]
        list[player:]] .. name .. [[;main;5.5,6.5;9,4]
        listring[]
        ]]
    local formspec = { new_formspec }

    minetest.show_formspec(name, "auctions:new", table.concat(formspec, ""))
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "auctions:new" then
        return
    end

    local name = player:get_player_name()
    local pinv = minetest.get_inventory({ type="player", name=name })
    local inv_name = "new:" .. name
    local inv = minetest.get_inventory({ type="detached", name=inv_name })
    local stack_items = inv:get_list("main")

    if fields.new then
        local items = {}
        -- convert itemstacks into tables
        for _, s in ipairs(stack_items) do
            table.insert(items, s:to_table())
        end
        if not inv:is_empty("main") and fields.title ~= "" and tonumber(fields.bid) > 0 and tonumber(fields.increment) > 0 then -- check required fields
            local duration = (tonumber(fields.days) or 0)*86400 + (tonumber(fields.hours) or 0)*3600 + (tonumber(fields.minuts) or 0)*60
            duration = math.max(duration, 600) -- min is 10 min
            duration = math.min(duration, 86400*14) -- max is 2 weeks
            local auction = {
                owner = name,
                title = fields.title,
                description = fields.desc,
                bid = tonumber(fields.bid),
                increment = tonumber(fields.increment),
                fee = tonumber(fields.fee),
                duration = duration,
                items = items
            }
            auctions.new_auction(auction)
            auctions.show_board(name)
            return true
        end
        return false
    elseif fields.back then
        auctions.show_board(name)
    end

    -- restore items to player inventory
    for _, s in ipairs(stack_items) do
        pinv:add_item("main", s)
        inv:remove_item("main", s)
    end

    return true
end)
