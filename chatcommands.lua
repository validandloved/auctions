minetest.register_chatcommand("auctions", {
    privs = {
        interact = true,
    },
    func = function(name, param)
        if #param > 0 then
            auctions.show_details(name, param:split(" ")[1])
        else
            auctions.show_board(name)
        end
        return true
    end,
})
